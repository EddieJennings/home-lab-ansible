# Home Lab - Ansible

Repository for my home lab's Ansible assets.

I am publishing it here for my own use and to give others ideas on things they may want to do with their home lab, Ansible, or both.

## New for the Summer 2024 Refresh

It seems like for the past few summers I've done a refresh of the configuration of my home lab / home production network.
Thanks to some Red Hat certifications earned earlier this year, I've decided to incorporate execution environments into my use of Ansible.

## Deploying the Ansible Control Node
Coming soon?

Since this environment is specific to me, a number of things would need to be tweaked to use it.
Perhaps when I get some more time, I'll make a separate document about general setup.

## License
Assets in this project are licensed under a [MIT license](LICENSE).

# Repository Structure
```
./
|-- files/ - General files used by Ansible assets ideally with subdirectories based on inventory hosts or groups
|-- inventory/ - Contains all inventory files
|-- playbooks/ - Contains all playbooks
    |-- hosts/ - Contains playbooks for specific managed nodes.  Named as inventory_entry.yml (usually FQDN)
```

# Hypervisor and DNS setup

## Hypervisor
As mentioned above, I'll make a separate document / actually think through documentation better soon(TM).

General steps for setting up the hypervisor for this environment (infra-kvm-01 running Fedora 40)
1. Setup a web server somewhere reachable that will have a kickstart file. Kickstart file for this environment isn't included in this repo...yet.
2. Start an installation of Fedora server on your hadware and set boot parameters to point to your kickstart file
3. On your workstation, add a hosts file entry for infra-kvm-01's FQDN.
4. On your workstation, clone this repo, and then run the infra-kvm-01 playbook in `playbooks/hosts/`
5. Very basic hypervisor configuration should be complete.

## DNS
1. Setup a web server somewhere reachable that will have a kickstart file. Kickstart file for this environment isn't included in this repo...yet.
2. Upload a Fedora Server iso to the `/kvm/isos/` directory on the hypervisor
3. Using Cockpit deploy a new virtual machine with these specifications
  - Network: Direct attached to `eno2` (must have Internet access)
  - Disk: Minimum of 15 GiB
  - Firmware: UEFI
  - VM autostarts at boot
4. Start the installation of the VM and interrupt the boot process to add these paramenters: `ip=1.1.1.1::2.2.2.2:3.3.3.3 inst.ks=http:/4.4.4.4/your-kickstart-file.ks`
  - 1.1.1.1 is the IP address of the DNS server which can reach the webserver with kickstart
  - 2.2.2.2 is the default gateway of the DNS server
  - 3.3.3.3 is the subnet mask for the DNS server
  - 4.4.4.4 is the IP address of the web server that has the kickstart file
5. On your workstation, add a hosts file entry for infra-bind-01's FQDN.
6. On your workstation, clone this repo, and then run the infra-bind-01 playbook in `playbooks/hosts/`
7. The DNS server is now deployed.
You may want to reboot the hypervisor (or bring down and up its network connection) so it will use the DNS server for name resolution.
