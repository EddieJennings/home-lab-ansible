Base
=========

This is the base role for the home lab environment.  Only inclulde configurations in this role if it should be applied to **EVERYTHING IN THE HOME LAB!**

Requirements
------------

Ideally this role should only use modules from the `ansible.builtin` collection.  Should the role expand beyond this, they should be configured as dependencies and documented here.

Role Variables
--------------

Current variables defined:
  - ansible_service_account (string): defines the name of the local account used as the Ansible service account as well as the name of some files
  - fedora_packages (list): Packages to be installed on all Fedora systems
  - packages_to_remove (list): Packages to be removed from all systems

Dependencies
------------

See requirements.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - base

License
-------

MIT

Author Information
------------------

Eddie Jennings, Jr.
