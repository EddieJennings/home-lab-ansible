dns_server Role
=========

Installs and configures BIND and zones

Requirements
------------

This role assumes Bind on Fedora is being used.

License
-------

MIT

Author Information
------------------

Eddie Jennings, Jr.
